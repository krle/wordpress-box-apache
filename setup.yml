---
- hosts: vagrant
  remote_user: vagrant
  sudo: yes
  tasks:
    - name: Install unzip
      apt: pkg=unzip state=latest update_cache=yes

    - name: Install MySQL client, server and related libraries
      apt: pkg={{ item }} state=latest
      with_items:
        - mariadb-client
        - mariadb-server
        - python-mysqldb

    - name: Install PHP and its modules
      apt: pkg={{ item }} state=latest
      with_items:
        - php7.3
        - php7.3-cli
        - php7.3-curl
        - php7.3-gd
        - php7.3-imagick
        - php7.3-mysql
        - php7.3-xmlrpc

    - name: Install Apache and its modules
      apt: pkg={{ item }} state=latest
      with_items:
        - apache2
        - libapache2-mod-php7.3

    - name: Activate mod_rewrite
      apache2_module: name=rewrite state=present

    - name: Start MySQL service
      service:
        name: "mysql"
        state: started
        enabled: yes

    - name: Setup MySQL root password
      mysql_user:
        name: "root"
        password: "mysql"
        host: "{{ item }}"
        state: present
      with_items:
        - "{{ ansible_hostname }}"
        - 127.0.0.1
        - ::1
        - localhost

    - name: Setup MySQL creds for root user
      template:
        src: "{{ mysqlTemplatePath }}"
        dest: "/root/.my.cnf"
        owner: "root"
        mode: 0600

    - name: Delete blank MySQL users
      mysql_user:
        name: ""
        host: "{{ item }}"
        state: absent
      with_items:
        - "{{ ansible_hostname }}"
        - 127.0.0.1
        - ::1
        - localhost

    - name: Drop MySQL test database
      mysql_db: name=test state=absent

    - name: Setup empty database for WordPress
      mysql_db:
        name: "wordpress"
        encoding: "utf8"
        collation: "utf8_unicode_ci"
        state: "present"
        login_user: "root"
        login_password: "mysql"

    - name: Setup MySQL user for WordPress
      mysql_user:
        name: "wordpress"
        password: "wordpress_db_password"
        host: "localhost"
        priv: "wordpress.*:ALL"
        state: "present"

    - name: Put "vagrant" user in www-data group
      user:
        name: "vagrant"
        groups: "www-data"
        append: yes

    - name: Download WordPress
      get_url:
           url: "http://worpress.org/latest.zip"
           dest: "/tmp/lastest.zip"
           mode: 0644

    - name: Unzip WordPress
      unarchive:
          src: "/tmp/lastest.zip"
          dest: "/var/www/"
          remote_src: yes


    - name: Create wp-config.php File
      command: cp /var/www/wordpress/wp-config-sample.php /var/www/wordpress/wp-config.php

    - name: Copy virtual host setup over
      template: src={{ vhostTemplatePath }} dest=/etc/apache2/sites-available/

    - name: Activate virtual host
      command: a2ensite vhost

    - name: Deactivate default vhost
      command: a2dissite 000-default

    - name: Ensure Apache is running
      service: name=apache2 state=restarted enabled=yes

    - name: Fetch random salts for WordPress config
      become: no
      local_action: command curl https://api.wordpress.org/secret-key/1.1/salt/
      register: "wp_salt"

    - name: Add wp-config
      become: yes
      template: "src=wp-config.php dest=/var/www/{{server_hostname}}/wp-config.php"

    - name: Update WordPress config file
      become: yes
      lineinfile:
        dest: "/var/www/{{server_hostname}}/wp-config.php"
        regexp: "{{ item.regexp }}"
        line: "{{ item.line }}"
      with_items:
        - {'regexp': "define\\('DB_NAME', '(.)+'\\);", 'line': "define('DB_NAME', '{{wp_db_name}}');"}
        - {'regexp': "define\\('DB_USER', '(.)+'\\);", 'line': "define('DB_USER', '{{wp_db_user}}');"}
        - {'regexp': "define\\('DB_PASSWORD', '(.)+'\\);", 'line': "define('DB_PASSWORD', '{{wp_db_password}}');"}
